package me.rrama.LavaSurvival;

import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class AchievementEvents implements Listener {
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntityDamage (EntityDamageEvent event) {
        if (Checks(event, RoundState.InGame)) {
            if (event.getEntity() instanceof Player) {
                Achievements.AwardAchievement(((Player)event.getEntity()).getName(), Achievements.Achievement.Scorched);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDeath (PlayerDeathEvent event) {
        if (Checks(RoundState.InGame)) {
            Achievements.AwardAchievement(event.getEntity().getName(), Achievements.Achievement.ided);
            if (Death.StillAlive.size() == 1) {
                Achievements.AwardAchievement(Death.StillAlive, Achievements.Achievement.Last_Man_Standing);
            }
        }
    }
    
    public static boolean Checks(final Cancellable event, final RoundState RS) {
        if (!event.isCancelled()) {
            return Checks(RS);
        } else return false;
    }
    
    public static boolean Checks(final RoundState RS) {
        if (RramaGaming.R == RS && "LavaSurvival".equals(RramaGaming.gameInPlay)) {
            return true;
        } else return false;
    }
}
