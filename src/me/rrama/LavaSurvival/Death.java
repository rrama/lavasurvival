package me.rrama.LavaSurvival;

import java.util.ArrayList;
import me.rrama.RramaGaming.ListToString;
import me.rrama.RramaGaming.Ref;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Death implements Listener {
    
    public static ArrayList<String> StillAlive = new ArrayList<>();
    public LavaSurvival plugin;

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player p = event.getEntity();
        String PN = p.getName();
        if (RramaGaming.gameInPlay.equals("LavaSurvival") && RramaGaming.R == RoundState.InGame && StillAlive.contains(PN)) {
            StillAlive.remove(PN);
            if (StillAlive.isEmpty() == true) {
                Bukkit.broadcastMessage(LavaSurvival.This.TagB + "The last person alive has died. Ending the round.");
                LavaSurvival.This.endRound(false);
            }
        }
    }
    
    public static class CMDs implements CommandExecutor {
        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
            if (commandLable.equalsIgnoreCase("Alive")) {
                if (RramaGaming.gameInPlay.equals("LavaSurvival") && RramaGaming.R == RoundState.InGame) {
                    if (StillAlive.isEmpty() == true) {
                        sender.sendMessage(ChatColor.YELLOW + "No one is alive? Round should have ended. Stopping it now.");
                        LavaSurvival.This.endRound(false);
                    } else {
                        String Survivors = ListToString.ListToString(StillAlive, ", ");
                        sender.sendMessage(ChatColor.YELLOW + "Players still alive: " + Survivors);
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You cannot use this command out side of rounds.");
                }
                return true;
            } else if (commandLable.equalsIgnoreCase("Resurrect") || commandLable.equalsIgnoreCase("Res")) {
                if (args.length == 1 && Bukkit.getPlayer(args[0]) != null) {
                    Player p = Bukkit.getPlayer(args[0]);
                    String PN = p.getName();
                    if (!RramaGaming.gameInPlay.equals("LavaSurvival")) {
                        sender.sendMessage(ChatColor.YELLOW + "lavaSurvival is not in play.");
                    } else if (RramaGaming.R != RoundState.InGame) {
                        sender.sendMessage(ChatColor.YELLOW + "Round is not in play.");
                    } else if (StillAlive.contains(PN)) {
                        sender.sendMessage(ChatColor.YELLOW + "This person is already still alive.");
                    } else if (Ref.Refs.contains(PN)) {
                        sender.sendMessage(ChatColor.YELLOW + "Refs cannot be made alive again.");
                    } else {
                        StillAlive.add(PN);
                        Bukkit.broadcastMessage(LavaSurvival.This.TagNo + ChatColor.GREEN + p.getName() + ChatColor.BLUE + " is Resurrected! Magic!" + ChatColor.MAGIC + "FuckingHackers");
                        Achievements.AwardAchievement(PN, Achievements.Achievement.Resurrected);
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}