package me.rrama.LavaSurvival;

import java.util.HashSet;
import java.util.List;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class FireControl implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onExtinguish(PlayerInteractEvent event) {
        if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (RramaGaming.gameInPlay.equals("LavaSurvival") && RramaGaming.R == RoundState.InGame) {
                Player p = event.getPlayer();

                HashSet<Byte> HS = new HashSet<>();
                HS.add(Byte.parseByte("51"));
                HS.add(Byte.parseByte("0"));
                List<Block> LB = p.getLastTwoTargetBlocks(HS, 1000);

                Block FireB2 = LB.get(0);
                if (FireB2.getTypeId() == 51) {
                    Block Sorrounded = LB.get(1);
                    for (BlockFace BF : LavaSpread.BFsAll) {
                        Block FireQ = Sorrounded.getRelative(BF);
                        if (FireQ.getTypeId() == 51) {
                            FireQ.setTypeId(0);
                        }
                    }
                }
            }
        }
    }
}