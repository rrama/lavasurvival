package me.rrama.LavaSurvival;

import java.io.File;
import me.rrama.LavaSurvival.Achievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

public class FolderSetup {
    
    public static FileConfiguration FC;
    public static File pluginFolder, PlayersAchievementsFolder;
    
    public static void FolderSetUp() {
        FC = LavaSurvival.This.getConfig();
        pluginFolder = LavaSurvival.This.getDataFolder();
        if (pluginFolder.exists() == false) {
            boolean b = pluginFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((LavaSurvival.This.TagB + "DataFolder failed to be made."));
            } else {
                Bukkit.getLogger().info((LavaSurvival.This.TagB + "Created a DataFolder for you :). rrama do good?"));
            }
            FolderSetUp();
        } else {
            FileSetUp();
            ConfigSetUp();
        }
    }
    
    public static void FileSetUp() {
        boolean repeat = false;
        final String LSep = System.getProperty("line.separator");
        final String FS = System.getProperty("file.separator");
        File FileLavaMaps = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "LavaMaps.txt", LavaSurvival.This.TagB, "#Format: Map name | Lava spawner coords" + LSep
                + "#Example: CoolMap_YEAH | 1, 70, -10");
        if (FileLavaMaps != null) {
            LavaMaps.InitializeMaps(FileLavaMaps);
        }

        PlayersAchievementsFolder = new File(pluginFolder, FS + "Achievements");
        if (PlayersAchievementsFolder.exists() == false) {
            boolean b = PlayersAchievementsFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((LavaSurvival.This.TagB + "Folder 'Achievements' failed to be made."));
            } else {
                Bukkit.getLogger().info((LavaSurvival.This.TagB + "Created an Achievements folder for you :). rrama do good?"));
            }
            repeat = true;
        } else {
            for (Achievement A : Achievement.values()) {
                me.rrama.RramaGaming.FolderSetup.CreateFile(PlayersAchievementsFolder, A + ".txt", LavaSurvival.This.TagB, null);
            }
        }

        File FileRules = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "Rules.txt", LavaSurvival.This.TagB, "No rules have been written here yet. Ask an OP for rules.");
        if (FileRules != null) {
            IntroAndRules.ReadRulesFromFile(FileRules);
        }

        File FileIntro = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "Intro.txt", LavaSurvival.This.TagB, "No intro has been written here yet.");
        if (FileIntro != null) {
            IntroAndRules.ReadIntroFromFile(FileIntro);
        }

        File FileItemPrices = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "ItemPrices.txt", LavaSurvival.This.TagB, "1 Sponge: 10" + LSep + "1 Heart: 1" + LSep + "1 Re-Birth: 50" + LSep + "1 Leather Helmet: 10" + LSep + "1 Leather Chestplate: 20" + LSep + "1 Leather Leggings: 12" + LSep + "1 Leather Boots: 10" + LSep + "1 Full Set of Leather Armour: 45" + LSep + "1 Stone Pickaxe: 2" + LSep + "1 Care Package: 20");
        if (FileItemPrices != null) {
            Shop.ItemPricesReg(FileItemPrices);
        }

        if (repeat) {
            FileSetUp();
        }
    }
    
    public static void ConfigSetUp() {
        if (!FC.isSet("Winning cookies")) {
            FC.set("Winning cookies", 10);
        }
        LavaSurvival.WinningCookies = FC.getInt("Winning cookies");
        
        if (!FC.isSet("Default Lava speed")) {
            FC.set("Default Lava speed", 40);
        }
        LavaSpread.Speed = FC.getInt("Default Lava speed");
    }
}
