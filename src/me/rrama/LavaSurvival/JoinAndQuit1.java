package me.rrama.LavaSurvival;

import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinAndQuit1 implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        
        if (!RramaGaming.gameInPlay.equals("LavaSurvival")) return;
        
        Player P = event.getPlayer();
        
        if (RramaGaming.R == RoundState.PreGame) {
            P.sendMessage(ChatColor.GOLD + "Round has not started yet. Build a shelter!");
        } else if (RramaGaming.R == RoundState.InGame) {
            P.sendMessage(ChatColor.GOLD + "Round has started. Please wait in spawn.");
        }
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {

        Player P = event.getPlayer();
        String PN = P.getName();

        if (Death.StillAlive.contains(PN)) {
            Death.StillAlive.remove(PN);
            if (Death.StillAlive.isEmpty()) {
                Bukkit.broadcastMessage(LavaSurvival.This.TagB + "The last person alive has left. Ending the round.");
                LavaSurvival.This.endRound(false);
            }
        }
    }
}