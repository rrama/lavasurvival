package me.rrama.LavaSurvival;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import me.rrama.RramaGaming.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.util.Vector;

public class LavaMaps {
    
    public static void InitializeMaps(final File F) {
        try (Scanner S = new Scanner(F)) {
            while (S.hasNextLine()) {
                String LS = S.nextLine();
                if (!LS.startsWith("#")) {
                    final String[] LSA = LS.split(" \\| ");
                    String[] LLSA = LSA[1].split(", ");
                    Vector Spawner = new Vector(Double.valueOf(LLSA[0]), Double.valueOf(LLSA[1]), Double.valueOf(LLSA[2]));
                    LavaSurvival.This.Maps.add(LSA[0]);
                    GamingMaps.GamingMaps.get(LSA[0]).addMetaData("LavaSurvival-Spawner", Spawner);
                }
            }
            if (LavaSurvival.This.Maps.isEmpty()) {
                Bukkit.getLogger().warning((LavaSurvival.This.TagB + "No mapz in da LavaMaps file!!! LavaSurvival plugin die now x_x ided."));
                Bukkit.getPluginManager().disablePlugin(LavaSurvival.This);
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((LavaSurvival.This.TagB + "Cannot find da LavaMaps file!!! LavaSurvival plugin die now x_x ided."));
            Bukkit.getPluginManager().disablePlugin(LavaSurvival.This);
        }
    }
}