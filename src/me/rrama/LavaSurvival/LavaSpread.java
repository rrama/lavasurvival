package me.rrama.LavaSurvival;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import me.rrama.RramaGaming.OpChat;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;
 
public class LavaSpread implements Listener {
    
    public static List<Location> SpongeBlocks = new ArrayList<>();
    public static List<Location> LavaBlocks = new ArrayList<>();
    public static List<Vector> LavaBlocksVec = new ArrayList<>();
    public static final List<BlockFace> BFsToSpread = Arrays.asList(BlockFace.DOWN, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST);
    public static final List<BlockFace> BFsAll = Arrays.asList(BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST);
    public static final List<Integer> Burnable = Arrays.asList(5, 17, 18, 35, 47);
    public static final List<Integer> Meltable = Arrays.asList(1, 2, 3, 4, 8, 9, 13, 20, 44, 49, 50, 53, 59, 60, 85, 102);
    public static BukkitTask SpreadTime;
    public static int Speed;
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlace(BlockPlaceEvent event) {
        
        Block block = event.getBlock();
        Location L = block.getLocation();

        if (block.getTypeId() == 10 || block.getTypeId() == 11) {
            if (me.rrama.RramaGaming.FolderSetup.CPUOverRAM) {
                LavaBlocks.add(L);
            } else {
                LavaBlocksVec.add(L.toVector());
            }
            
        } else if (block.getTypeId() == 19) {
            final Location L2 = L.clone().subtract(2.0, 2.0, 2.0);
            final Location Lf = L.clone().add(3.0, 3.0, 3.0);
            for (int x = L2.getBlockX(); x > Lf.getBlockX(); x++) {
                for (int y = L2.getBlockY(); y > Lf.getBlockY(); y++) {
                    for (int z = L2.getBlockZ(); z > Lf.getBlockZ(); z++) {
                        Location LDone = new Location(Bukkit.getWorlds().get(0), x, y, z);
                        if (block.getTypeId() == 8 || block.getTypeId() == 9 || block.getTypeId() == 10 || block.getTypeId() == 11) {
                            LDone.getBlock().setTypeId(0);
                        }
                        SpongeBlocks.add(LDone);
                    }
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        
        Block block = event.getBlock();
        Location L = block.getLocation();
        
        if (block.getTypeId() == 10 || block.getTypeId() == 11) {
            if (me.rrama.RramaGaming.FolderSetup.CPUOverRAM) {
                LavaBlocks.remove(L);
            } else {
                LavaBlocksVec.remove(L.toVector());
            }
        } else if (block.getTypeId() == 19) {
            Player P = event.getPlayer();
            P.sendMessage(ChatColor.GOLD + "One does not simply break a sponges.");
            event.setCancelled(true);
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
        
        ItemStack IS = event.getItemStack();
        int Id = IS.getTypeId();
        Location L = event.getBlockClicked().getRelative(event.getBlockFace()).getLocation();
        
        if (Id == 327) {
            if (me.rrama.RramaGaming.FolderSetup.CPUOverRAM) {
                LavaBlocks.remove(L);
            } else {
                LavaBlocksVec.remove(L.toVector());
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        
        final Location L = event.getBlockClicked().getRelative(event.getBlockFace()).getLocation();
        int Id = event.getPlayer().getItemInHand().getTypeId();
        
        if (Id == 327) {
            if (me.rrama.RramaGaming.FolderSetup.CPUOverRAM) {
                LavaBlocks.add(L);
            } else {
                LavaBlocksVec.add(L.toVector());
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onFromToEvent(BlockFromToEvent event) {
        if ("LavaSurvival".equals(RramaGaming.gameInPlay)) {
            int Id = event.getBlock().getTypeId();
            int Id2 = event.getToBlock().getTypeId();
            if (Id == 10 || Id == 11 || Id2 == 10 || Id2 == 11){
                event.setCancelled(true);
            }
        }
    }
    
    public static void SpreaderTime(final int FSpeed) {
        if (me.rrama.RramaGaming.FolderSetup.CPUOverRAM) {
            SpreadTime = Bukkit.getScheduler().runTaskTimer(LavaSurvival.This, new Runnable() {

                @Override
                public void run() {
                    Spreader(LavaBlocks);
                }
            }, FSpeed, FSpeed);
        } else {
            SpreadTime = Bukkit.getScheduler().runTaskTimer(LavaSurvival.This, new Runnable() {

                @Override
                public void run() {
                    SpreaderVec(LavaBlocksVec);
                }
            }, FSpeed, FSpeed);
        }
    }
    
    public static void ResetSpreadTime(final boolean Restart) {
        if (SpreadTime != null) {
            SpreadTime.cancel();
        }
        if (Restart == true) {
            SpreaderTime(Speed);
        }
    }
    
    public static void Spreader(final List<Location> LavaBlocksToUse) {
        List<Location> LavaBlocksToRemove = new ArrayList<>();
        List<Location> LavaBlocksToAdd = new ArrayList<>();
        
        for (final Location l : LavaBlocksToUse) {
            final Block block = l.getBlock();
            
            if (SpongeBlocks.contains(l)) {
                if (block.getTypeId() == 10 || block.getTypeId() == 11) {
                    block.setTypeId(0);
                }
                if (!LavaBlocksToRemove.contains(l)) {
                    LavaBlocksToRemove.add(l);
                }
            } else {
                
                for (BlockFace BF : BFsToSpread) {
                    final Block B = block.getRelative(BF);
                    final int BId = B.getTypeId();

                    if (BId == 10) {
//                        if (!LavaBlocksToAdd.contains(B.getLocation())) {
//                            if (!LavaBlocks.contains(B.getLocation())) {
//                                LavaBlocksToAdd.add(B.getLocation());
//                            }
//                        }
                    } else if (BId == 0) {
                        if (!SpongeBlocks.contains(B.getLocation())) {
                            B.setTypeId(10);
                            LavaBlocksToAdd.add(B.getLocation());
                        }
                    } else if (BId == 11) {
                        B.setTypeId(10);
//                        if (!LavaBlocks.contains(B.getLocation())) {
//                            LavaBlocksToAdd.add(B.getLocation());
//                        }
                    } else if (Meltable.contains(BId)) { //meltables to air
                        final int rand = LavaSurvival.generator.nextInt(10);
                        if (rand == 1) {
                            B.setTypeId(0);
                        }
                    } else if (Burnable.contains(BId)) {
                        final int rand = LavaSurvival.generator.nextInt(10);
                        if (rand == 1) {
                            boolean AllAir = true;
                            for (BlockFace BF2 : BFsAll) {
                                Block block2 = B.getRelative(BF2);
                                if (block2.getTypeId() == 0) {
                                    block2.setTypeId(51);
                                    AllAir = false;
                                }
                            }
                            if (AllAir) {
                                B.setTypeId(0);
                            }
                        }
                    } else if (BId == 12) { //sand to glass
                        B.setTypeId(20);
                    }
                }

            }
        }
        LavaBlocks.addAll(LavaBlocksToAdd);
        LavaBlocks.removeAll(LavaBlocksToRemove);
    }
    
    public static void SpreaderVec(final List<Vector> LavaBlocksToUse) {
        List<Vector> LavaBlocksToRemove = new ArrayList<>();
        List<Vector> LavaBlocksToAdd = new ArrayList<>();
        
        for (final Vector v : LavaBlocksToUse) {
            final Location l = new Location(Bukkit.getWorlds().get(0), v.getX(), v.getY(), v.getZ());
            final Block block = l.getBlock();
            
            if (SpongeBlocks.contains(l)) {
                if (block.getTypeId() == 10 || block.getTypeId() == 11) {
                    block.setTypeId(0);
                }
                if (!LavaBlocksToRemove.contains(v)) {
                    LavaBlocksToRemove.add(v);
                }
            } else {
                
                for (BlockFace BF : BFsToSpread) {
                    final Block B = block.getRelative(BF);
                    final int BId = B.getTypeId();

                    if (BId == 10) {
//                        if (!LavaBlocksToAdd.contains(B.getLocation().toVector())) {
//                            if (!LavaBlocks.contains(B.getLocation())) {
//                                LavaBlocksToAdd.add(B.getLocation());
//                            }
//                        }
                    } else if (BId == 0) {
                        if (!SpongeBlocks.contains(B.getLocation())) {
                            B.setTypeId(10);
                            LavaBlocksToAdd.add(B.getLocation().toVector());
                        }
                    } else if (BId == 11) {
                        B.setTypeId(10);
//                        if (!LavaBlocks.contains(B.getLocation())) {
//                            LavaBlocksToAdd.add(B.getLocation());
//                        }
                    } else if (Meltable.contains(B.getTypeId())) { //meltables to air
                        final int rand = LavaSurvival.generator.nextInt(10);
                        if (rand == 1) {
                            B.setTypeId(0);
                        }
                    } else if (Burnable.contains(B.getTypeId())) {
                        final int rand = LavaSurvival.generator.nextInt(10);
                        if (rand == 1) {
                            boolean AllAir = true;
                            for (BlockFace BF2 : BFsAll) {
                                Block block2 = B.getRelative(BF2);
                                if (block2.getTypeId() == 0) {
                                    block2.setTypeId(51);
                                    AllAir = false;
                                }
                            }
                            if (AllAir) {
                                B.setTypeId(0);
                            }
                        }
                    } else if (B.getTypeId() == 12) { //sand to glass
                        B.setTypeId(20);
                    }
                }

            }
        }
        LavaBlocksVec.addAll(LavaBlocksToAdd);
        LavaBlocksVec.removeAll(LavaBlocksToRemove);
    }
    
    public static class LavaSpeedCMD implements CommandExecutor {
        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
            if (commandLable.equalsIgnoreCase("LavaSpeed")) {
                if (args.length == 0) {
                    sender.sendMessage(ChatColor.YELLOW + "Lava speed is " + Speed + " TperB.");
                    return true;
                } else if (args.length == 1 && args[0].matches("-?\\d+(.\\d+)?")) {
                    int TSpeed = Integer.parseInt(args[0]);
                    if (TSpeed > 0) {
                        Speed = TSpeed;
                        if (RramaGaming.gameInPlay.equals("LavaSurvival") && RramaGaming.R == RoundState.InGame) {
                            ResetSpreadTime(true);
                        } else {
                            ResetSpreadTime(false);
                        }
                        OpChat.SendMessageToAllOps("LavaSurvival", sender.getName() + " changed the lava speed.");
                        Bukkit.broadcastMessage(LavaSurvival.This.TagB + "Changed the lava speed to " + Speed + " TperB.");
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "Speed has to be greater than 0.");
                    }
                    return true;
                } else return false;
            } else return false;
        }
    }
    
    public static class LavaBlocksCMD implements CommandExecutor {
        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
            if (commandLable.equalsIgnoreCase("LavaBlocks")) {
                int LavaBlocksSize;
                if (me.rrama.RramaGaming.FolderSetup.CPUOverRAM) {
                    LavaBlocksSize = LavaBlocks.size();
                } else {
                    LavaBlocksSize = LavaBlocksVec.size();
                }
                sender.sendMessage(ChatColor.YELLOW + "Amount of lava blocks: " + LavaBlocksSize);
                return true;
            } else return false;
        }
    }
}