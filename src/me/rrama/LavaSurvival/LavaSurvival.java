package me.rrama.LavaSurvival;

/**
 *
 * @author rrama
 * All rights reserved to rrama.
 * No copying/stealing any part of the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * No copying/stealing ideas from the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * 
 * Based on classic McLava by LISTINGS09, classic Mc7ime by 7imekeeper and classic IgneusCraft by the Derplin Dev team.
 * @credits Credit goes to rrama (author), LISTINGS09 (inspiration), 7imekeeper (inspiration), the Derplin Dev team (inspiration)
 */

import java.util.Random;
import me.rrama.RramaGaming.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class LavaSurvival extends GamingPlugin {
    
    public static Random generator = new Random();
    public static int WinningCookies;
    public static GamingPlugin This;
 
    @Override
    public void onGameDisable() {
        Bukkit.getScheduler().cancelTasks(This);
    }
 
    @Override
    public void onGameEnable() {
        This = this;
        MinimumPlayersNeeded = 1;
        FolderSetup.FolderSetUp();
        RegisterEvents.Register();
        
        getCommand("Alive").setExecutor(new Death.CMDs());
        getCommand("Resurrect").setExecutor(new Death.CMDs());
        getCommand("LavaSpeed").setExecutor(new LavaSpread.LavaSpeedCMD());
        getCommand("LavaBlocks").setExecutor(new LavaSpread.LavaBlocksCMD());
        getCommand("LavaIntro").setExecutor(new IntroAndRules());
        getCommand("LavaRules").setExecutor(new IntroAndRules());
        getCommand("LavaTime").setExecutor(new TimedTasks.TimeCommand());
        getCommand("AddLavaTime").setExecutor(new RIPTimer.TimeCommand());
        getCommand("LavaSurvivalAchievements").setExecutor(new AchievementCommands());
    }
    
    @Override
    public void startPreGame() {
        LavaSpread.LavaBlocks.clear();
        LavaSpread.LavaBlocksVec.clear();
        LavaSpread.SpongeBlocks.clear();
        MaxHeight.BuildHeight = ((Vector)RramaGaming.MapInPlay.getMetaData().get("LavaSurvival-Spawner")).getBlockY();
        TimedTasks.TimerMain();
    }
    
    @Override
    public void startRound() {
        for (Player p : Bukkit.getWorlds().get(0).getPlayers()) {
            RramaGaming.WasInRound.add(p.getName());
        }
        RIPTimer.TimerMain();
        for (Player p : Bukkit.getWorlds().get(0).getPlayers()) {
            Death.StillAlive.add(p.getName());
        }
        Death.StillAlive.removeAll(Ref.Refs);
        for (String s : Death.StillAlive) {
            Player p = Bukkit.getPlayerExact(s);
            p.setHealth(p.getMaxHealth());
        }
        Location lbs = ((Vector) RramaGaming.MapInPlay.getMetaData().get("LavaSurvival-Spawner")).toLocation(Bukkit.getWorlds().get(0));
        lbs.getBlock().setTypeId(10);
        if (me.rrama.RramaGaming.FolderSetup.CPUOverRAM) {
            LavaSpread.LavaBlocks.add(lbs);
        } else {
            LavaSpread.LavaBlocksVec.add(lbs.toVector());
        }
        RramaGaming.MapInPlay = GamingMaps.GamingMaps.get("Spawn");
        LavaSpread.SpreaderTime(LavaSpread.Speed);
        Bukkit.broadcastMessage(This.TagB + "Round has started!");
    }
    
    @Override
    public void endRound(final boolean FS) {
        LavaSpread.ResetSpreadTime(false);
        System.gc();
        if (Death.StillAlive.isEmpty() == true) {
            Bukkit.broadcastMessage(This.TagB + "Everyone has died!");
        } else {
            String Survivors = ListToString.ListToString(Death.StillAlive, ", ");
            Bukkit.broadcastMessage(This.TagB + "Round ended. Well done to all the survivors: " + ChatColor.GREEN + Survivors);
            for (String s : Death.StillAlive) {
                try {
                    RramaGaming.getGamer(s).addCookies(WinningCookies);
                    Bukkit.getPlayerExact(s).sendMessage(ChatColor.GOLD + "You got " + WinningCookies + " cookies for surviving :)");
                } catch (CookieFileException ex) {
                    ex.sendConsoleMessage();
                    ex.sendPlayerMessage();
                }
            }
            Achievements.AwardAchievement(Death.StillAlive, Achievements.Achievement.Survivor);
            if (Death.StillAlive.size() == 1) {
                Achievements.AwardAchievement(Death.StillAlive, Achievements.Achievement.Lone_Survivor);
            }
        }
        Death.StillAlive.clear();
        System.gc();
    }
}