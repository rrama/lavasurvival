package me.rrama.LavaSurvival;

import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class Pain implements Listener {
    
    public LavaSurvival plugin;

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamage(EntityDamageEvent event) {
        Entity e = event.getEntity();
        if (e instanceof Player && RramaGaming.gameInPlay.equals("LavaSurvival") && RramaGaming.R == RoundState.InGame) {
            Player p = (Player)e;
            String PN = p.getName();
            DamageCause DC = event.getCause();
            if (DC == DamageCause.LAVA && Death.StillAlive.contains(PN)) {
                event.setCancelled(false);
                event.setDamage(1);
            }
        }
    }
}