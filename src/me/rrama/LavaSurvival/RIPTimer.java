package me.rrama.LavaSurvival;

import me.rrama.RramaGaming.OpChat;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import me.rrama.RramaGaming.Timer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class RIPTimer {
    
    public static void TimerMain() {
        if (RramaGaming.gameInPlay.equals("LavaSurvival") && RramaGaming.R == RoundState.InGame) {
            new Timer(LavaSurvival.This, true, 365);
        } else {
            Bukkit.broadcastMessage(LavaSurvival.This.TagB + "TimerMain() was launched when LavaSurvival was not in play or R != InGame.");
        }
    }
    
    public static class TimeCommand implements CommandExecutor {

        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
            if (commandLable.equalsIgnoreCase("AddLavaTime")) {
                if (RramaGaming.gameInPlay.equals("LavaSurvival") && RramaGaming.R == RoundState.PreGame) {
                    if (LavaSurvival.This.timer.getTime() < 20) {
                        sender.sendMessage(ChatColor.YELLOW + "It is too late to extend the building time.");
                    } else if (args.length == 1 && args[0].matches("-?\\d+(.\\d+)?")) {
                        final int time2add = Integer.parseInt(args[0]);
//                        LavaSurvival.This.timer.Time += time2add;
                        OpChat.SendMessageToAllOps("LavaSurvival", sender.getName() + " added " + time2add + " seconds to the time.");
                        Bukkit.broadcastMessage(LavaSurvival.This.TagB + "The building time has been extended by " + ChatColor.RED + time2add + " seconds" + ChatColor.BLUE + ".");
                    } else return false;
                    
                } else if (RramaGaming.gameInPlay.equals("LavaSurvival") && RramaGaming.R == RoundState.InGame) {
                    if (LavaSurvival.This.timer.getTime() < 20) {
                        sender.sendMessage(ChatColor.YELLOW + "It is too late to extend the round.");
                    } else if (args.length == 1 && args[0].matches("-?\\d+(.\\d+)?")) {
                        final int time2add = Integer.parseInt(args[0]);
//                        LavaSurvival.This.timer.Time += time2add;
                        OpChat.SendMessageToAllOps("LavaSurvival", sender.getName() + " added " + time2add + " seconds to the time.");
                        Bukkit.broadcastMessage(LavaSurvival.This.TagB + "The round has been extended by " + ChatColor.RED + time2add + " seconds" + ChatColor.BLUE + ".");
                    } else return false;
                    
                } else {
                    Bukkit.dispatchCommand(sender, "LavaTime");
                }
                return true;
            } else {
                return false;
            }
        }
        
    }
}