package me.rrama.LavaSurvival;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class RegisterEvents {
    
    public static void Register () {
        //PluginManager
        final PluginManager pm = Bukkit.getServer().getPluginManager();
        
        //Event Classes
        final AchievementEvents AchievementEventsEvent = new AchievementEvents();
        final FireControl FireControlEvent = new FireControl();
        final JoinAndQuit1 JoinAndQuit1Class = new JoinAndQuit1();
        final LavaSpread LavaSpreadClass = new LavaSpread();
        final Pain PainClass = new Pain();
        final Death DeathClass = new Death();
        final Refing RefingClass = new Refing();
        
        //Register Them
        pm.registerEvents(FireControlEvent, LavaSurvival.This);
        pm.registerEvents(LavaSpreadClass, LavaSurvival.This);
        pm.registerEvents(JoinAndQuit1Class, LavaSurvival.This);
        pm.registerEvents(DeathClass, LavaSurvival.This);
        pm.registerEvents(PainClass, LavaSurvival.This);
        pm.registerEvents(RefingClass, LavaSurvival.This);
        pm.registerEvents(AchievementEventsEvent, LavaSurvival.This);
    }
    
}
