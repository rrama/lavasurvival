package me.rrama.LavaSurvival;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import me.rrama.LavaSurvival.ShopItems.*;
import me.rrama.RramaGaming.ShopItems.ShopCarePackage;
import org.bukkit.Bukkit;

public class Shop {
    
    public static void ItemPricesReg(final File FileItemPrices) {
        try {
            Scanner s = new Scanner(FileItemPrices);
            s.reset();
            int count = 5;
            while (s.hasNextLine()) {
                String S = s.nextLine();
                String[] Sy = S.split(": ");
                if (Sy.length != 2) {
                    Bukkit.getLogger().warning((LavaSurvival.This.TagB + "The line '" + S + "' does not follow the correct format for 'ItemPrices.txt'."));
                } else if (!Sy[1].matches("-?\\d+(.\\d+)?")) {
                    Bukkit.getLogger().warning((LavaSurvival.This.TagB + "'" + Sy[1] + "' in '" + S + "' in 'ItemPrices.txt' is not an integer."));
                } else {
                    String Name = Sy[0];
                    int ItemPrice = Integer.parseInt(Sy[1]);
                    //IndiItems reg
                    if (count == 5) {
                        new Sponge(LavaSurvival.This, Name, ItemPrice);
                    } else if (count == 6) {
                        new Cookie(LavaSurvival.This, Name, ItemPrice);
                    } else if (count == 7) {
                        new ReBirth(LavaSurvival.This, Name, ItemPrice);
                    } else if (count == 8) {
                        new LeatherHelmet(LavaSurvival.This, Name, ItemPrice);
                    } else if (count == 9) {
                        new LeatherChestplate(LavaSurvival.This, Name, ItemPrice);
                    } else if (count == 10) {
                        new LeatherLeggings(LavaSurvival.This, Name, ItemPrice);
                    } else if (count == 11) {
                        new LeatherBoots(LavaSurvival.This, Name, ItemPrice);
                    } else if (count == 12) {
                        new LeatherFull(LavaSurvival.This, Name, ItemPrice);
                    } else if (count == 13) {
                        new StonePickaxe(LavaSurvival.This, Name, ItemPrice);
                    } else if (count == 14) {
                        new ShopCarePackage(LavaSurvival.This, Name, ItemPrice);
                    }
                    //IndiItems reg end
                }
                count++;
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((LavaSurvival.This.TagB + "Could not find File 'ItemPrices.txt' even though it exists."));
        }
    }
}