package me.rrama.LavaSurvival.ShopItems;

import me.rrama.LavaSurvival.Death;
import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.ShopItems.ShopItem;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class AliveShopItem extends ShopItem {
    
    public AliveShopItem(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean isVisibleTo(final Player P) {
        return Death.StillAlive.contains(P.getName());
    }
    
    @Override
    public boolean canBuy(final Player P) {
        if (!Death.StillAlive.contains(P.getName())) {
            P.sendMessage(ChatColor.YELLOW + "You must be alive to buy " + getName() + ".");
            return false;
        }
        return true;
    }
    
}