package me.rrama.LavaSurvival.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LeatherBoots extends AliveShopItem {
    
    public LeatherBoots(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean perform(final Player P) {
        ItemStack IS = new ItemStack(Material.LEATHER_BOOTS, 1);
        return P.getInventory().addItem(IS).isEmpty();
    }
    
}