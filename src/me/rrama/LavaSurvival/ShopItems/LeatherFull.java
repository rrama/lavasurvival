package me.rrama.LavaSurvival.ShopItems;

import me.rrama.LavaSurvival.LavaSurvival;
import me.rrama.RramaGaming.GamingPlugin;
import org.bukkit.entity.Player;

public class LeatherFull extends AliveShopItem {
    
    public LeatherFull(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean perform(final Player P) {
        boolean b = true;
        for (int i = 1; i < 5; ++i) {
            if (!LavaSurvival.This.ShopItems.get(i).perform(P)) {
                b = false;
            }
        }
        return b;
    }
    
}