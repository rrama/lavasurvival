package me.rrama.LavaSurvival.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LeatherLeggings extends AliveShopItem {
    
    public LeatherLeggings(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean perform(final Player P) {
        ItemStack IS = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        return P.getInventory().addItem(IS).isEmpty();
    }
    
}